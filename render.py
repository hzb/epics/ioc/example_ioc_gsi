from jinja2 import Environment, FileSystemLoader, select_autoescape
import re

def render_dockerfile(template_path, output_path, context):
    env = Environment(loader=FileSystemLoader("."), autoescape=select_autoescape(['j2']))
    template = env.get_template(template_path)
    rendered_template = template.render(context)

    # Remove empty lines from the rendered template
    rendered_lines = rendered_template.splitlines()
    non_empty_lines = filter(lambda line: line.strip() != '', rendered_lines)
    cleaned_template = '\n'.join(non_empty_lines)

    with open(output_path, "w") as f:
        f.write(cleaned_template)

if __name__ == "__main__":
    CONTEXTS = {
  '2.0.0': { 'base_ver': '7.0.6',
             'asyn_ver': (4,42),
             'as_ver': 'R5-10-2',
             'calc_ver': 'R3-7-4',
             'modbus_ver': (3,2),
             'snmp_ver': (1,1,0,2),
             'stream_ver': (2,8,22),
             'devthmpledp_ver': 'v1.0.0',
             'iocstats_ver': 'cpu-temp',
             'ftdi_support': 'yes',
           },
  '2.0.0-sbc': { 'base_ver': '7.0.6',
                 'asyn_ver': (4,42),
                 'as_ver': 'R5-10-2',
                 'calc_ver': 'R3-7-4',
                 'modbus_ver': (3,2),
                 'snmp_ver': (1,1,0,2),
                 'stream_ver': (2,8,22),
                 'drvasyni2c_ver': 'R1-0-3b',
                 'devgpio_ver': 'R2-0-1',
                 'devthmpledp_ver': 'v1.0.0',
                 'iocstats_ver': 'cpu-temp',
                 'ftdi_support': 'yes',
              },
}

    template_file = "Dockerfile.jinja2"

    for context_name, context_values in CONTEXTS.items():
        output_file = f"Rendered_Dockerfile_{context_name}.Dockerfile"
        render_dockerfile(template_file, output_file, context_values)
